<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="Lava_Escape_0"></a>Lava Escape</h1>
<hr>
<h1 class="code-line" data-line-start=2 data-line-end=3 ><a id="__2"></a>Суть игры:</h1>
<p class="has-line-data" data-line-start="3" data-line-end="6">Игра-платформер. Игрок должен успеть добраться до верхней платформы, уничтожая врагов и избегая лавы, которая также<br>
медленно поднимается вверх.<br>
Условия поражения:</p>
<ul>
<li class="has-line-data" data-line-start="6" data-line-end="7">Игрока настигла лава</li>
<li class="has-line-data" data-line-start="7" data-line-end="9">Игрок потерял все здоровье от противников</li>
</ul>
<p class="has-line-data" data-line-start="9" data-line-end="10">Условие победы:</p>
<ul>
<li class="has-line-data" data-line-start="10" data-line-end="12">Игрок успел добраться до финальной платформы</li>
</ul>
<h1 class="code-line" data-line-start=12 data-line-end=13 ><a id="_12"></a>Геймплей:</h1>
<p class="has-line-data" data-line-start="13" data-line-end="14">Геймплей представлен в двух видео win.mkv и lose.mkv с победными и проигрышными забегами соответственно</p>
